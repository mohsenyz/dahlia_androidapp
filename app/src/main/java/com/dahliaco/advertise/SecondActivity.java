package com.dahliaco.advertise;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.*;
import android.support.v7.app.*;
import android.support.v7.widget.*;
import android.text.Html;
import android.view.*;
import android.support.design.widget.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.support.v4.app.*;
import android.widget.TextView;

import com.dahliaco.advertise.fragment.InfoDialogFragment;
import com.dahliaco.advertise.helper.NumberFormatting;
import com.squareup.picasso.Picasso;

public class SecondActivity extends AppCompatActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		//overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
		final Bundle b = getIntent().getExtras();
		Picasso.with(this).load((String)b.getCharSequence("image")).placeholder(R.drawable.ic_action_name).error(R.drawable.ic_action_name).into((ImageView)findViewById(R.id.image));
		final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		collapsingToolbar.setTitleEnabled(false);
		AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
		appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
			@Override
			public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
				int y = Math.abs(verticalOffset) * 100 / appBarLayout.getTotalScrollRange();
				int alpha = 255 * y / 100;
				alpha = 255 - alpha;
				GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{Color.TRANSPARENT,Color.argb(alpha, 50, 50, 50)});
				toolbar.setBackgroundDrawable(g);
			}
		});

		Typeface tf = Typeface.createFromAsset(this.getAssets(), "iran.ttf");
		collapsingToolbar.setExpandedTitleTypeface(tf);
		collapsingToolbar.setCollapsedTitleTypeface(tf);
		TextView title  = (TextView) findViewById(R.id.title);
		title.setText(b.getCharSequence("sub"));
		TextView location = (TextView) findViewById(R.id.location);
		location.setText(Html.fromHtml("<b>محل: </b>" + (String) b.getCharSequence("city")));
		TextView time = (TextView) findViewById(R.id.time);
		time.setText(Html.fromHtml("<b>زمان: </b>" + "لحظاتی پیش"));
		TextView content = (TextView) findViewById(R.id.content);
		content.setText(b.getCharSequence("content"));
		TextView price =  (TextView) findViewById(R.id.price);
		TextView trust = (TextView) findViewById(R.id.trust);
		RelativeLayout price_parent = (RelativeLayout) findViewById(R.id.price_parent);
		RelativeLayout trust_parent = (RelativeLayout) findViewById(R.id.trust_parent);

		if(b.containsKey("price")){
			price.setText(Html.fromHtml("<font color=\"#1b5e20\">" + NumberFormatting.englishToArabic((String) b.getCharSequence("price")) + "</font> تومان"));
		}else{
			price_parent.setVisibility(View.GONE);
		}
		if(b.containsKey("trust")){
			trust.setText(Html.fromHtml("<font color=\"#1b5e20\">" + NumberFormatting.englishToArabic((String)b.getCharSequence("trust")) + "</font> تومان"));
		}else{
			trust_parent.setVisibility(View.GONE);
		}
		FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentManager fm = getSupportFragmentManager();
				InfoDialogFragment editNameDialogFragment = InfoDialogFragment.newInstance((String)b.getCharSequence("phone"),(String)b.getCharSequence("email"), (String)b.getCharSequence("tel"));
				editNameDialogFragment.show(fm, "اطلاعات");
			}
		});
		overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }
}
