package com.dahliaco.advertise;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FiltersActivityDialog extends AppCompatActivity {
    public static final int ACTIVITY_CODE = 1212;
    public static final String ACTIVITY_KEY = "location";
    private ImageView imageView;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
        setContentView(R.layout.filters_activity_dialog);
        Intent i = getIntent();
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(FiltersActivityDialog.ACTIVITY_KEY, "hello from my activity");
                setResult(FiltersActivityDialog.ACTIVITY_CODE,i);
                finish();
            }
        });
        toolbarTitle.setText(i.getStringExtra("title"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        /*btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(FiltersActivityDialog.ACTIVITY_KEY, "hello from my activity");
                setResult(FiltersActivityDialog.ACTIVITY_CODE,i);
                finish();
            }
        });*/

    }
}
