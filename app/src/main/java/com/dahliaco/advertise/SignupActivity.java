package com.dahliaco.advertise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dahliaco.advertise.helper.NumberFormatting;
import com.dahliaco.advertise.widget.DahliaTimer;

public class SignupActivity extends AppCompatActivity {
    private Button continueBtn;
    private Button cancelBtn;
    private long exitTime = 0;
    private TextView text_enterphone ,text_entercode;
    private DahliaTimer text_timer;
    private EditText edittext_phone, edittext_code;
    private Animation move_out_right, move_in_left, fade_in, fade_out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        move_out_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_out_right);
        move_in_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_in_left);
        fade_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        fade_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        edittext_phone = (EditText) findViewById(R.id.edittext_phone);
        edittext_code = (EditText) findViewById(R.id.edittext_code);
        text_enterphone = (TextView) findViewById(R.id.text_enterphone);
        text_timer = (DahliaTimer) findViewById(R.id.timer);
        text_timer.setOnFinishListener(new DahliaTimer.onFinishListener() {
            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(),"dsgsdgsg",Toast.LENGTH_SHORT).show();
            }
        });
        text_entercode = (TextView) findViewById(R.id.text_entercode);
        continueBtn = (Button) findViewById(R.id.continuebtn);
        edittext_phone.requestFocus();
        move_out_right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                text_enterphone.setVisibility(View.GONE);
                text_entercode.setVisibility(View.VISIBLE);
                text_entercode.startAnimation(move_in_left);
                edittext_phone.setVisibility(View.GONE);
                edittext_code.setVisibility(View.VISIBLE);
                edittext_code.startAnimation(move_in_left);
                edittext_code.requestFocus();
                continueBtn.setText(getResources().getText(R.string.submit));
                text_timer.setVisibility(View.VISIBLE);
                text_timer.setText(NumberFormatting.englishToArabic("1:30"));
                text_timer.startAnimation(fade_in);
                text_timer.setMaxTime(90);
                text_timer.startTimer();
                fade_out.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        cancelBtn.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                cancelBtn.startAnimation(fade_out);
                fade_out.reset();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        cancelBtn = (Button) findViewById(R.id.cancelbtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_timer.finishTimer();
                finish();
                /*if(System.currentTimeMillis() - exitTime < 1000){
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),getResources().getText(R.string.pressagaintoexit),Toast.LENGTH_SHORT).show();
                    exitTime = System.currentTimeMillis();
                }*/
            }
        });
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueBtn.setOnClickListener(null);
                text_enterphone.startAnimation(move_out_right);
                move_out_right.reset();
                edittext_phone.startAnimation(move_out_right);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(System.currentTimeMillis() - exitTime < 2000){
            finish();
            text_timer.finishTimer();
        }else{
            Toast.makeText(getApplicationContext(),getResources().getText(R.string.pressagaintoexit),Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        }
    }

    @Override
    protected void onPause() {
        text_timer.finishTimer();
        super.onPause();
    }
}
