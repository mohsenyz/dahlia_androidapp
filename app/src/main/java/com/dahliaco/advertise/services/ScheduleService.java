package com.dahliaco.advertise.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dahliaco.advertise.AppController;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.ConnectionDetector;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleService extends Service {
    public ScheduleService() {
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        final ScheduledExecutorService sch = Executors.newSingleThreadScheduledExecutor();
        sch.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if(ConnectionDetector.isConnectedToInternet()){
                    StringRequest str = new StringRequest(Request.Method.GET, Config.URL_GET_ADVERTISE, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String s) {
                            Log.i("dahlia-Response", s.substring(1,10) + "Time : " + new SimpleDateFormat("hh:mm:ss").format(new Date()));
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Log.i("dahlia-Error", volleyError.getMessage().substring(1, 10) + System.currentTimeMillis());
                            Log.i("dahlia-Error", "dahlia-error" + new SimpleDateFormat("hh:mm:ss").format(new Date()));
                        }
                    }){
                        @Override
                        public Priority getPriority() {
                            return Priority.LOW;
                        }
                    };
                    str.setShouldCache(false);
                    AppController.getInstance().addToRequestQueue(str);
                }
            }
        }, 10, 30, TimeUnit.SECONDS);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
