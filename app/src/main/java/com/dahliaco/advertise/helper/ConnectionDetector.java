package com.dahliaco.advertise.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dahliaco.advertise.AppController;

/**
 * Created by dahlia on 8/25/16.
 */
public class ConnectionDetector {
    public static boolean isConnectedToInternet(){
        ConnectivityManager mgr = (ConnectivityManager) AppController.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(mgr != null){
            NetworkInfo[] info = mgr.getAllNetworkInfo();
            if(info != null){
                for (int i = 0; i < info.length; i++){
                    if(info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
                }
            }
        }
        return false;
    }
}
