package com.dahliaco.advertise.helper;

import com.dahliaco.advertise.AppController;

import java.io.InputStream;

/**
 * Created by dahlia on 9/4/16.
 */
public class DahliaAssetManager {
    public static byte[] get(String name){
        byte[] fileBytes = null;
        try{
            InputStream is= AppController.getInstance().getAssets().open(name);
            fileBytes=new byte[is.available()];
            is.read(fileBytes);
            is.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return fileBytes;
    }
}
