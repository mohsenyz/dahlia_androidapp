package com.dahliaco.advertise.helper;

/**
 * Created by dahlia on 9/2/16.
 */
public class ValidatorHelper {
    public static boolean isEmpty(String s){
        return s.trim().isEmpty();
    }

    public static boolean between(String s, int... i){
        return (s.length() > i[0] && s.length() < i[1]);
    }

    public static boolean moreThanOrEqual(String s, int i){
        return (s.toString().length() >= i);
    }
}
