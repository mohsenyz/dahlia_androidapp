package com.dahliaco.advertise.helper;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by dahlia on 9/3/16.
 */
public class MiniDb{
    SharedPreferences s;
    SharedPreferences.Editor e;

    public MiniDb(Context c){
        s = c.getSharedPreferences("minidb", Context.MODE_PRIVATE);
        e = s.edit();
    }

    public void add(String a, boolean b){
        e.putBoolean(a, b);
        e.commit();
    }

    public void add(String a, String b){
        e.putString(a, b);
        e.commit();
    }

    public void add(String a, int b){
        e.putInt(a, b);
        e.commit();
    }

    public void add(String a, float b){
        e.putFloat(a, b);
        e.commit();
    }

    public boolean getBoolean(String a){
        return s.getBoolean(a, false);
    }

    public String getString(String a){
        return s.getString(a, null);
    }

    public int getInt(String a){
        return s.getInt(a, -1);
    }

    public float getFloat(String a){
        return s.getFloat(a, -1);
    }

    public boolean issetKey(String a){
        return s.contains(a);
    }
}
