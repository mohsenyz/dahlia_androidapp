package com.dahliaco.advertise.helper;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by dahlia on 8/26/16.
 */
public class PreCacheLayoutManager extends LinearLayoutManager {
    private Context _context;
    private static final int EXTRA = 600;
    private int extra = -1;

    public PreCacheLayoutManager(Context c){
        super(c);
        this._context = c;
    }

    public PreCacheLayoutManager(Context c, int defextra){
        super(c);
        this.extra = defextra;
        this._context = c;
    }

    public PreCacheLayoutManager(Context c, int d, boolean a){
        super(c, d, a);
        this._context = c;
    }

    public void setExtraLayoutSpace(int defextra){
        this.extra = defextra;
    }

    @Override
    protected int getExtraLayoutSpace(RecyclerView.State state){
        if(this.extra > 0){
            return this.extra;
        }
        return EXTRA;
    }
}
