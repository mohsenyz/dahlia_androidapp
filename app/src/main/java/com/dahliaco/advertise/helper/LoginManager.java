package com.dahliaco.advertise.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dahlia on 9/1/16.
 */
public class LoginManager {

    SharedPreferences shared;
    SharedPreferences.Editor editor;
    private static final String NAME = "session";
    private static final String USERNAME_FIELD = "username";
    private static final String PASSWORD_FIELD = "password";

    public LoginManager(Context c){
        shared = c.getSharedPreferences(NAME, 0);
        editor = shared.edit();
    }

    public void login(String username, String password){
        editor.putString(USERNAME_FIELD, username);
        editor.putString(PASSWORD_FIELD, password);
        editor.commit();
    }

    public boolean isLogin(){
        return (shared.getString(USERNAME_FIELD, null) != null);
    }

    public void logout(){
        editor.clear();
        editor.commit();
    }
}
