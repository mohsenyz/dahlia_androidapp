package com.dahliaco.advertise.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;

/**
 * Created by dahlia on 9/3/16.
 */
public class BitmapLoader {
    public static class LoadBitmapFromTemp extends AsyncTask<String, Void, Bitmap> {
        OnBitmapLoadListener l;
        public LoadBitmapFromTemp(OnBitmapLoadListener b){
            this.l = b;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            byte[] b = TempManager.get(params[0]);
            if(b != null){
                Log.i("gee", "Dsgsdgg");
                return BitmapFactory.decodeByteArray(b, 0, b.length);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap b) {
            this.l.onBitmapLoad(b);
        }
    }

    public static class LoadBitmapIntoTemp extends AsyncTask<Bitmap, Void, Boolean> {
        String name;
        Bitmap.CompressFormat f;
        public LoadBitmapIntoTemp(String b, Bitmap.CompressFormat g){
            this.name = b;
            this.f = g;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Bitmap... params) {
            ByteArrayOutputStream o = new ByteArrayOutputStream();
            params[0].compress(f, 100, o);
            TempManager.put(name, o.toByteArray());
            return true;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
        }
    }

    public interface OnBitmapLoadListener{
        public void onBitmapLoad(Bitmap b);
    }
}
