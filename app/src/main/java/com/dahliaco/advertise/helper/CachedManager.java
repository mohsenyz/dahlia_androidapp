package com.dahliaco.advertise.helper;

import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.records.CachedAdvertise;
import com.orm.query.Select;

/**
 * Created by dahlia on 8/24/16.
 */
public class CachedManager {
    public static void addAdvertise(ListObj obj){
        new CachedAdvertise(obj.getId(), obj.getDate(), obj.getTitle(), obj.getContent(), obj.getCategory(), obj.getImageId(), obj.getPhone(), obj.getEmail(), obj.getTel(), obj.getCity(), obj.getPrice()).save();
    }

    public static boolean issetAdvertise(int id){
        return (CachedAdvertise.count(CachedAdvertise.class, "advertise_id = ?", new String[]{Integer.toString(id)}) > 0);
    }

    public static void removeAdvertise(int id){
        CachedAdvertise.deleteAll(CachedAdvertise.class, "advertise_id = ?", new String[]{Integer.toString(id)});
    }

    public static CachedAdvertise getAdvertise(int id){
        return Select.from(CachedAdvertise.class).where("advertise_id = ?", new String[]{Integer.toString(id)}).first();
    }
}
