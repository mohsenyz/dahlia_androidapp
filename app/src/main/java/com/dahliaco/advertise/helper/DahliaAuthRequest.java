package com.dahliaco.advertise.helper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dahlia on 9/4/16.
 */
public class DahliaAuthRequest extends Request<NetworkResponse> {

    private final LoginSession login;
    private final Response.Listener<NetworkResponse> listener;


    public DahliaAuthRequest(String url, LoginSession _login,
                       Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.login = _login;
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if(this.login != null){
            Map<String, String > m = new HashMap<>();
            m.put("Cookie", this.login.get());
            return m;
        }
        return super.getHeaders();
    }

    @Override
    protected void deliverResponse(NetworkResponse response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        return Response.success(
                response,
                HttpHeaderParser.parseCacheHeaders(response));
    }


    public static class LoginSession{
        private String a;

        public LoginSession(String _a){
            a = _a;
        }

        public void set(String _a){
            this.a = _a;
        }

        public String get(){
            return a;
        }
    }
}
