package com.dahliaco.advertise.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.dahliaco.advertise.records.LikedAdvertise;
import com.orm.query.Condition;
import com.orm.query.Select;

/**
 * Created by dahlia on 8/23/16.
 */
public class LikeManager {
    public static void addAdvertise(int id){
        new LikedAdvertise(id).save();
    }
    public static boolean issetAdvertise(int id){
        return (LikedAdvertise.count(LikedAdvertise.class, "key = ?", new String[]{Integer.toString(id)}) > 0);
    }
    public static void removeAdvertise(int id){
        LikedAdvertise.deleteAll(LikedAdvertise.class, "key = ?", Integer.toString(id));
    }
}
