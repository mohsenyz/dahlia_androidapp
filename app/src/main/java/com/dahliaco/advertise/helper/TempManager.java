package com.dahliaco.advertise.helper;

import android.util.Log;

import com.dahliaco.advertise.AppController;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by dahlia on 9/3/16.
 */
public class TempManager {
    public static void put(String name, byte[] data){
        try {
            FileOutputStream fOut = AppController.getInstance().openFileOutput(name,AppController.getInstance().MODE_WORLD_READABLE);
            fOut.write(data);
            fOut.flush();
            fOut.close();
            data = null;
        }

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static byte[] get(String name){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try{
            FileInputStream fin = AppController.getInstance().openFileInput(name);
            byte[] b = new byte[1024];
            int a;
            while ((a = fin.read(b)) != -1) {
                bos.write(b, 0, a);
            }
        }
        catch(Exception e){
        }
        return bos.toByteArray();
    }

    public static boolean isset(String name){
        for (String a : AppController.getInstance().fileList()){
            if(a.equals(name))
                return true;
        }
        return false;
    }

    public static File[] getAll(){
        File[] b = null;
        for (String a : AppController.getInstance().fileList()){
            b[b.length] = new File(AppController.getInstance().getFilesDir(), a);
        }
        return b;
    }

    public static boolean delete(String name){
        if(isset(name)){
            AppController.getInstance().deleteFile(name);
            return true;
        }
        return false;
    }
}
