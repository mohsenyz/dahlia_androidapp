package com.dahliaco.advertise.helper;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Created by dahlia on 8/23/16.
 */
public class MultiScreens {
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
