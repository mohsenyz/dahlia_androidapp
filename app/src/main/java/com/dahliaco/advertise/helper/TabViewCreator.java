package com.dahliaco.advertise.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dahliaco.advertise.R;
import com.squareup.picasso.Picasso;

/**
 * Created by dahlia on 8/22/16.
 */
public class TabViewCreator {
    private Context c;
    private LayoutInflater l;
    private View v;
    private String text;
    private int resid;
    public TabViewCreator(Context m){
        this.c = m;
        l = LayoutInflater.from(m);
        v = l.inflate(R.layout.main_tab_layout, null);
    }
    public TabViewCreator setText(String _text){
        this.text = _text;
        return this;
    }
    public TabViewCreator setImage(int res){
        this.resid = res;
        return this;
    }
    public View getView(){
        TextView txt = (TextView) v.findViewById(R.id.text);
        ImageView image = (ImageView) v.findViewById(R.id.image);
        Picasso.with(this.c).load(this.resid).into(image);
        txt.setText(this.text);
        return v;
    }
}
