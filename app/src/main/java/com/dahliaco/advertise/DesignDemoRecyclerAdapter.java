package com.dahliaco.advertise;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.List;
import android.content.*;
import android.widget.Toast;

import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.LikeManager;
import com.dahliaco.advertise.helper.MultiScreens;
import com.dahliaco.advertise.helper.NumberFormatting;
import com.squareup.picasso.Picasso;
import com.dahliaco.advertise.R;
/**
 * Created by echessa on 7/24/15.
 */
public class DesignDemoRecyclerAdapter extends RecyclerView.Adapter<DesignDemoRecyclerAdapter.ViewHolder> {

    private Handler h = new Handler();
    public List<ListObj> mItems;
    public static PopupWindow popupWindow = null;
    private boolean isInit = false;
    private OnLongClickListener onLongClick;
    private OnClickListener onClick;
    private OnSelectStateChangedListener onChange;
    private OnItemSelectStateChangedListener onItemSelectStateChanged;
    public static boolean IS_SELECTED = false;
    public void setIsSelected(boolean b){
        IS_SELECTED = b;
        if(this.onChange != null){
            this.onChange.onSelectStateChanged(b);
        }else if(AppController.onSelectStateChangedListener != null){
            AppController.onSelectStateChangedListener.onSelectStateChanged(b);
        }
    }

    public void setOnItemSelectStateListener(OnItemSelectStateChangedListener b){
        this.onItemSelectStateChanged = b;
    }
    public DesignDemoRecyclerAdapter(List<ListObj> items) {
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, viewGroup, false);
        if(!isInit){
            isInit = true;
            LayoutInflater layoutInflater
                    = (LayoutInflater)viewGroup.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View popupView = layoutInflater.inflate(R.layout.list_item_menu, null);
            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final ListObj item = mItems.get(i);
        if(IS_SELECTED){
            viewHolder.checkBox.setVisibility(View.VISIBLE);
            if(item.getChecked()){
                viewHolder.checkBox.setChecked(true);
                viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card_press));
            }else{
                viewHolder.checkBox.setChecked(false);
                viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card));
            }
        }else {
            item.setChecked(false);
            viewHolder.checkBox.setChecked(false);
            viewHolder.checkBox.setVisibility(View.GONE);
            viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card));
        }
		viewHolder.mTextView.setText(item.getTitle());
        viewHolder.priceView.setText(NumberFormatting.englishToArabic(item.getPrice()) + " " + "تومان");
        Picasso.with(viewHolder.imageView.getContext()).load(item.getImageUrl()).placeholder(R.drawable.profile_list_default).error(R.drawable.profile_list_default).into(viewHolder.imageView);
        viewHolder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick.onLongClick(v, item, i);
                h.removeCallbacksAndMessages(null);
                popupWindow.getContentView().findViewById(R.id.like).setBackgroundResource(R.drawable.ic_action_name);
                popupWindow.getContentView().findViewById(R.id.like).setScaleX((float) 1);
                popupWindow.getContentView().findViewById(R.id.like).setScaleY((float) 1);
                if(LikeManager.issetAdvertise(item.getId())){
                    popupWindow.getContentView().findViewById(R.id.like).setBackgroundResource(R.drawable.liked);
                    popupWindow.getContentView().findViewById(R.id.like).setScaleX((float) 0.8);
                    popupWindow.getContentView().findViewById(R.id.like).setScaleY((float) 0.8);

                }
                //init events
                popupWindow.getContentView().findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        String shareBody = item.getTitle() + "\n" + v.getContext().getResources().getString(R.string.readmoreat) + "\n" + Config.APP_URL + "fa/view/" + item.getId() + "\n" + v.getContext().getResources().getString(R.string.dahliawebsite);
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, v.getContext().getResources().getString(R.string.advertise));
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                        v.getContext().startActivity(Intent.createChooser(sharingIntent, v.getContext().getResources().getString(R.string.sharevia)));
                    }
                });
                final Animation move_in_left = AnimationUtils.loadAnimation(v.getContext(), R.anim.like_scale);
                popupWindow.getContentView().findViewById(R.id.like).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ImageView popupLike = (ImageView) popupWindow.getContentView().findViewById(R.id.like);
                        if(LikeManager.issetAdvertise(item.getId())){
                            popupLike.setBackgroundResource(R.drawable.ic_action_name);
                            popupLike.setScaleX((float) 1);
                            popupLike.setScaleY((float) 1);
                            LikeManager.removeAdvertise(item.getId());
                        }else{
                            popupLike.setBackgroundResource(R.drawable.liked);
                            move_in_left.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    popupLike.setScaleY((float)0.8);
                                    popupLike.setScaleX((float)0.8);
                                    move_in_left.setAnimationListener(null);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            popupLike.startAnimation(move_in_left);
                            LikeManager.addAdvertise(item.getId());
                        }
                    }
                });
                //finish
                popupWindow.dismiss();
                popupWindow.showAsDropDown(v);
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        popupWindow.dismiss();
                    }
                }, 6000);
                return false;
            }
        });
		viewHolder.card.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
                    onClick.onClick(view, item, i);
					if (!IS_SELECTED){
                        Context context = view.getContext();
                        Intent i = new Intent(context, SecondActivity.class);
                        boolean isTablet = context.getResources().getBoolean(R.bool.isTablet);
                        if (MultiScreens.isTablet(context) || isTablet) {
                            i = new Intent(context, SecondActivityDialog.class);
                        }
                        i.putExtra("content", item.getContent());
                        i.putExtra("sub", item.getTitle());
                        i.putExtra("price", item.getPrice());
                        i.putExtra("image", item.getImageUrl());
                        i.putExtra("city", "اصفهان");
                        i.putExtra("phone", item.getPhone());
                        i.putExtra("email", item.getEmail());
                        i.putExtra("tel", item.getTel());
                        context.startActivity(i);
                    }else{
                        if(item.getChecked()){
                            viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card));
                            item.setChecked(false);
                            if(onItemSelectStateChanged != null){
                                onItemSelectStateChanged.onItemSelectStateChange(item, view, false);
                            }
                            viewHolder.checkBox.setChecked(false);
                        }else {
                            viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card_press));
                            item.setChecked(true);
                            if(onItemSelectStateChanged != null){
                                onItemSelectStateChanged.onItemSelectStateChange(item, view, true);
                            }
                            viewHolder.checkBox.setChecked(true);
                        }
                    }
                    //Toast.makeText(context, item.getPhone() + "-" + item.getContent() + "-" + item.getImageUrl(),Toast.LENGTH_SHORT).show();
				}
			});

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card_press));
                    item.setChecked(true);
                }else {
                    viewHolder.card.setForeground(viewHolder.card.getContext().getResources().getDrawable(R.drawable.card));
                    item.setChecked(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTextView;
        private final TextView priceView;
        private final ImageView imageView;
        private final CheckBox checkBox;
        private final CardView card;
        ViewHolder(View v) {
            super(v);
            card = (CardView) v.findViewById(R.id.card);
            mTextView = (TextView)v.findViewById(R.id.list_item);
            priceView = (TextView)v.findViewById(R.id.price);
            imageView = (ImageView)v.findViewById(R.id.image);
            checkBox = (CheckBox)v.findViewById(R.id.check);
        }
    }


    public void setOnClickListener(OnClickListener o){
        onClick = o;
    }

    public void setOnLongClickListener(OnLongClickListener o){
        onLongClick = o;
    }

    public void setOnSelectStateChangedListener(OnSelectStateChangedListener o){
        onChange = o;
    }

    public interface OnLongClickListener{
        public void onLongClick(View v, ListObj o, int i);
    }

    public interface OnClickListener{
        public void onClick(View v, ListObj o, int i);
    }

    public interface OnSelectStateChangedListener{
        public void onSelectStateChanged(boolean b);
    }

    public interface OnItemSelectStateChangedListener{
        public void onItemSelectStateChange(ListObj o, View v, boolean b);
    }
}
