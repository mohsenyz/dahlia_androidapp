package com.dahliaco.advertise;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.List;
import android.content.*;
import android.widget.Toast;

import com.dahliaco.advertise.adapter.AuctionObj;
import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.LikeManager;
import com.dahliaco.advertise.helper.MultiScreens;
import com.dahliaco.advertise.helper.NumberFormatting;
import com.dahliaco.advertise.widget.TypewriterView;
import com.squareup.picasso.Picasso;
import com.dahliaco.advertise.R;
/**
 * Created by echessa on 7/24/15.
 */
public class AuctionListAdapter extends RecyclerView.Adapter<AuctionListAdapter.ViewHolder> {

    private List<AuctionObj> mItems;
    public AuctionListAdapter(List<AuctionObj> items) {
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.auction_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final AuctionObj item = mItems.get(i);
        viewHolder.subject.setText(item.getTitle());
        viewHolder.content.setText(item.getContent());
        viewHolder.location.setText(viewHolder.location.getContext().getResources().getText(R.string.auctionisavailablein) + "  " +  "اصفهان");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewHolder.news.type(viewHolder.news.getContext().getResources().getText(R.string.demo_auction_price)).pause(2000).delete(viewHolder.news.getContext().getResources().getText(R.string.demo_auction_price)).pause(3000).type(viewHolder.news.getContext().getResources().getText(R.string.demo_auction_num)).pause(2000).delete(viewHolder.news.getContext().getResources().getText(R.string.demo_auction_num));
                new Handler().postDelayed(this, 18000);
            }
        }, 1000);
        Picasso.with(viewHolder.imageView.getContext()).load(item.getImageUrl()).placeholder(R.drawable.ic_action_name).error(R.drawable.ic_action_name).into(viewHolder.imageView);
        viewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent i = new Intent(context, SecondActivity.class);
                boolean isTablet = context.getResources().getBoolean(R.bool.isTablet);
                if (MultiScreens.isTablet(context) || isTablet) {
                    i = new Intent(context, SecondActivityDialog.class);
                }
                i.putExtra("content", item.getContent());
                i.putExtra("sub", item.getTitle());
                i.putExtra("price", item.getPrice());
                i.putExtra("image", item.getImageUrl());
                i.putExtra("city", "اصفهان");
                i.putExtra("phone", item.getPhone());
                i.putExtra("email", item.getEmail());
                i.putExtra("tel", item.getTel());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;
        private final CardView card;
        private final TypewriterView news;
        private final TextView subject, location, content;
        ViewHolder(View v) {
            super(v);
            card = (CardView) v.findViewById(R.id.card);
            imageView = (ImageView)v.findViewById(R.id.image);
            news = (TypewriterView) v.findViewById(R.id.news);
            subject = (TextView) v.findViewById(R.id.subject);
            location = (TextView) v.findViewById(R.id.location);
            content = (TextView) v.findViewById(R.id.content);
        }
    }

}
