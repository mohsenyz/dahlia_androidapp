package com.dahliaco.advertise;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.dahliaco.advertise.helper.FontsOverride;
import com.dahliaco.advertise.helper.LruBitmapCache;
import com.orm.SugarContext;

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    public static DesignDemoRecyclerAdapter adapter;
    public static DesignDemoRecyclerAdapter.OnSelectStateChangedListener onSelectStateChangedListener;
    public static DesignDemoRecyclerAdapter.OnItemSelectStateChangedListener onItemSelectStateChangedListener;
    private RequestQueue mRequestQueue;

    private static AppController mInstance;
    private ImageLoader imageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        mInstance = this;
        FontsOverride.setDefaultFont(this, "MONOSPACE", "iran.ttf");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader(){
        getRequestQueue();
        if(imageLoader == null){
            imageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.imageLoader;
    }
}
