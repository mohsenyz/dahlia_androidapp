package com.dahliaco.advertise.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dahliaco.advertise.fragment.AdvertiseFragment;
import com.dahliaco.advertise.fragment.AuctionFragment;
import com.dahliaco.advertise.fragment.ProfileFragment;

/**
 * Created by dahlia on 8/2/16.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {
    private Context m;
    public MainPagerAdapter(FragmentManager fm, Context s) {
        super(fm);
        this.m = s;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return ProfileFragment.newInstance(position);
        }
        if(position == 1){
            return AuctionFragment.newInstance(position);
        }
        if(position == 2){
            return AdvertiseFragment.newInstance(position);
        }
        return AdvertiseFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return ProfileFragment.TAB_NAME;
        }
        if(position == 1){
            return AuctionFragment.TAB_NAME;

        }
        if(position == 2){
            return AdvertiseFragment.TAB_NAME;
        }
        return AdvertiseFragment.TAB_NAME;
    }
}
