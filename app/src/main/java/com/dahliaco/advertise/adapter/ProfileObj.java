package com.dahliaco.advertise.adapter;

/**
 * Created by dahlia on 8/30/16.
 */
public class ProfileObj {
    private String title;
    private int image;

    public ProfileObj(String t, int i){
        this.title = t;
        this.image = i;
    }

    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
