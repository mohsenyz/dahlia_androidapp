package com.dahliaco.advertise.adapter;

/**
 * Created by dahlia on 8/24/16.
 */
public class AuctionObj {
    private String title;
    private String price;
    private String imageId;
    private int id;
    private String content;
    private String phone;
    private String email;
    private String tel;
    private String category;
    private String city;
    private int date;

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getDate() {
        return date;
    }

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String _email){
        this.email = _email;
    }
    public String getTel(){
        return this.tel;
    }
    public void setTel(String _tel){
        this.tel = _tel;
    }
    public String getCity() {
        return city;
    }

    public String getPhone() {
        return phone;
    }

    public void setCity(String _city) {
        this.city = _city;
    }

    public void setPhone(String _phone){
        this.phone = _phone;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int _id){
        this.id = _id;
    }

    public void setContent(String _content){
        this.content = _content;
    }

    public String getContent(){
        return this.content;
    }

    public void setTitle(String _title){
        this.title = _title;
    }

    public String getTitle() {
        return title;
    }

    public void setPrice(String _price) {
        this.price = _price;
    }

    public String getPrice() {
        return price;
    }

    public void setImageId(String id){
        this.imageId = id;
    }

    public String getImageId() {
        return imageId;
    }

    public String getImageUrl(){
        return "https://drive.google.com/uc?export=view&id=" + this.imageId;
    }
}
