package com.dahliaco.advertise;

import android.app.SearchManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.*;
import android.support.v4.view.*;
import android.support.v4.widget.*;
import android.support.v7.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.support.design.widget.*;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import android.content.*;
import android.util.*;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Map;
import com.android.volley.toolbox.*;
import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.adapter.MainPagerAdapter;
import com.dahliaco.advertise.conf.*;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.BitmapLoader;
import com.dahliaco.advertise.helper.DahliaAssetManager;
import com.dahliaco.advertise.helper.DahliaAuthRequest;
import com.dahliaco.advertise.helper.MiniDb;
import com.dahliaco.advertise.helper.NumberFormatting;
import com.dahliaco.advertise.helper.TabViewCreator;
import com.dahliaco.advertise.helper.TempManager;
import com.dahliaco.advertise.services.ScheduleService;


public class MainActivity extends AppCompatActivity
{
	
	private DrawerLayout mDrawerLayout;
	public Context con;

	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		startService(new Intent(MainActivity.this, ScheduleService.class));
		overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final Toolbar selected_toolbar = (Toolbar) findViewById(R.id.selected_toolbar);
        final TextView selected_text = (TextView) findViewById(R.id.selected_title);
        final Button selected_button = (Button) findViewById(R.id.selected_button);
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		final NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
				@Override
				public boolean onNavigationItemSelected(MenuItem menuItem) {
					menuItem.setChecked(true);
					mDrawerLayout.closeDrawers();
					Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_LONG).show();
					return true;
				}
			});
		final FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this, ActivityCreateAdvertise.class);
                    startActivity(i);
				}
			});
		MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), getApplicationContext());
		ViewPager viewPager = (ViewPager)findViewById(R.id.viewpager);
		viewPager.setAdapter(adapter);
		final TabLayout tabLayout = (TabLayout)findViewById(R.id.tablayout);
        final TabLayout tabLayout2 = (TabLayout)findViewById(R.id.tablayout2);
        tabLayout2.setVisibility(View.GONE);
		final Typeface tf = Typeface.createFromAsset(this.getAssets(), "iran.ttf");
        tabLayout.addTab(tabLayout.newTab().setCustomView(new TabViewCreator(this).setText(getResources().getString(R.string.morefilter)).setImage(R.drawable.ic_filter).getView()));
		tabLayout.addTab(tabLayout.newTab().setCustomView(new TabViewCreator(this).setText(getResources().getString(R.string.urgent)).setImage(R.drawable.ic_urgent).getView()));
		tabLayout.addTab(tabLayout.newTab().setCustomView(new TabViewCreator(this).setText(getResources().getString(R.string.categorie)).setImage(R.drawable.ic_category).getView()));
		tabLayout.addTab(tabLayout.newTab().setCustomView(new TabViewCreator(this).setText(getResources().getString(R.string.location)).setImage(R.drawable.ic_location).getView()));


		tabLayout2.addTab(tabLayout2.newTab().setCustomView(new TabViewCreator(this).setText(getResources().getString(R.string.location)).setImage(R.drawable.ic_location).getView()));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Toast.makeText(getApplicationContext(),tab.getText(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this, FiltersActivityDialog.class);
                i.putExtra("title", tab.getText());
                startActivityForResult(i, FiltersActivityDialog.ACTIVITY_CODE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
		TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
		toolbarTitle.setTypeface(tf, Typeface.NORMAL);
		con = this;
        viewPager.setCurrentItem(3);

		final TabLayout tab1 = (TabLayout) findViewById(R.id.tablayout1);
		tab1.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position == 2){
                    fab.setVisibility(View.VISIBLE);
                    tabLayout.setVisibility(View.VISIBLE);
                    tabLayout2.setVisibility(View.GONE);
                }
                if(position == 1){
                    fab.setVisibility(View.VISIBLE);
                    tabLayout.setVisibility(View.GONE);
                    tabLayout2.setVisibility(View.VISIBLE);
                }
                if(position == 0){
                    fab.setVisibility(View.GONE);
                    tabLayout.setVisibility(View.GONE);
                    tabLayout2.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });



		//saveProfileAccount();


        AppController.onSelectStateChangedListener = new DesignDemoRecyclerAdapter.OnSelectStateChangedListener() {
            @Override
            public void onSelectStateChanged(boolean b) {
                if(b){
                    selected_text.setText(getResources().getString(R.string.itemselected) + " (" + NumberFormatting.englishToArabic("0") + ")");
                    selected_toolbar.setVisibility(View.VISIBLE);
                    tabLayout.setVisibility(View.GONE);
				}else {
                    tabLayout.setVisibility(View.VISIBLE);
                    selected_toolbar.setVisibility(View.GONE);
				}
            }
        };
        selected_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabLayout.setVisibility(View.VISIBLE);
                selected_toolbar.setVisibility(View.GONE);
                AppController.adapter.setIsSelected(false);
                AppController.adapter.notifyDataSetChanged();
            }
        });
        AppController.onItemSelectStateChangedListener = new DesignDemoRecyclerAdapter.OnItemSelectStateChangedListener() {
            @Override
            public void onItemSelectStateChange(ListObj o, View v, boolean b) {
                int num = 0;
                for (ListObj ob : AppController.adapter.mItems){
                    if(ob.getChecked()) {
                        num++;
                    }
                }
                selected_text.setText(getResources().getString(R.string.itemselected) + " (" + NumberFormatting.englishToArabic(Integer.toString(num)) + ")");
            }
        };


        if(!new MiniDb(this).getBoolean(Config.MINIDB_NOT_FIRST_VIEW) || !TempManager.isset(".drawer")){
            new MiniDb(getApplicationContext()).add(Config.MINIDB_NOT_FIRST_VIEW, true);
            final ImageRequest m = new ImageRequest(Config.URL_GET_DRAWER_VIEW, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap bitmap) {
                    new BitmapLoader.LoadBitmapIntoTemp(".drawer", Bitmap.CompressFormat.PNG).execute(bitmap);
                    ((ImageView)navigationView.getHeaderView(0).findViewById(R.id.mimage)).setImageBitmap(bitmap);
                }
            }, 500, 500, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                }
            }){
                @Override
                public Priority getPriority(){
                    return Priority.LOW;
                }
            };
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AppController.getInstance().getRequestQueue().add(m);
                }
            }, 2000);
        }else{
            new BitmapLoader.LoadBitmapFromTemp(new BitmapLoader.OnBitmapLoadListener() {
                @Override
                public void onBitmapLoad(Bitmap b) {
                    if (b != null){
                        ((ImageView)navigationView.getHeaderView(0).findViewById(R.id.mimage)).setImageBitmap(b);
                    }
                }
            }).execute(".drawer");
        }



        if(!TempManager.isset(".category") || !TempManager.isset(".category_sub") || !TempManager.isset(".category_kind")){
            TempManager.put(".category_kind", DahliaAssetManager.get("kindCategory.json"));
            TempManager.put(".category_sub", DahliaAssetManager.get("subCategory.json"));
            TempManager.put(".category", DahliaAssetManager.get("getCategory.json"));
        }

    }


	@Override
	public void onBackPressed() {
		if(AppController.adapter.IS_SELECTED){
			AppController.adapter.setIsSelected(false);
			AppController.adapter.notifyDataSetChanged();
		}else {
			super.onBackPressed();
		}
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

		SearchManager searchManager =
				(SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView =
				(SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(
				searchManager.getSearchableInfo(getComponentName()));
		searchView.setQueryHint("جستجو");
		searchView.setMaxWidth(2000);
        TextView searchText = (TextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"iran.ttf");
        searchText.setTypeface(myCustomFont);
		searchText.setTextSize(14);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				Toast.makeText(getApplicationContext(), "So hello from the outside", Toast.LENGTH_SHORT).show();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {

				return false;
			}
		});

		return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FiltersActivityDialog.ACTIVITY_CODE){
            Toast.makeText(getApplicationContext(),data.getStringExtra(FiltersActivityDialog.ACTIVITY_KEY),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
	protected void onNewIntent(Intent intent) {

		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {

		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);

		}
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as youspecify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		switch (id) {
			case R.id.menu:
				mDrawerLayout.openDrawer(Gravity.RIGHT);
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	
	
	private void saveProfileAccount() {
        // loading or check internet connection or something...
        // ... then
		Response.Listener<NetworkResponse> t = new Response.Listener<NetworkResponse>(){
			@Override
			public void onResponse(NetworkResponse r){
				Log.d("worked","workes" + r.statusCode + new String(r.data));
			}
		};
		Response.ErrorListener e = new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {

				// Error handlin
				System.out.println("Something went wrong!");
				error.printStackTrace();

			}
		};
        String url = "http://localhost:8080/q.php";
		
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, t, e) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_token", "gh659gjhvdyudo973823tt9gvjf7i6ric75r76");
				params.put("m","hello from the outside");
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageViews
                params.put("avatar", new DataPart("file_avatar.jpg",new String("hello from the otherside").getBytes()));

                return params;
            }
        };

		Volley.newRequestQueue(this).add(multipartRequest);
    }
	
}
