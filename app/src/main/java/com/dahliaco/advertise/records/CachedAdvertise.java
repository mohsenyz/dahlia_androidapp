package com.dahliaco.advertise.records;

import com.orm.SugarRecord;

/**
 * Created by dahlia on 8/23/16.
 */
public class CachedAdvertise extends SugarRecord {
    private int advertiseId;
    private int advertiseDate;
    private String advertiseSub;
    private String advertiseContent;
    private String advertiseCategory;
    private String advertisePic;
    private String advertisePhone;
    private String advertiseEmail;
    private String advertiseTelegram;
    private String advertiseCity;
    private String advertisePrice;

    public CachedAdvertise(){

    }

    public CachedAdvertise(int id, int date, String sub, String content, String category, String pic, String phone, String email, String telegram, String city, String price){
        this.advertiseId = id;
        this.advertiseDate = date;
        this.advertiseSub = sub;
        this.advertiseContent = content;
        this.advertiseCategory = category;
        this.advertisePic = pic;
        this.advertisePhone = phone;
        this.advertiseEmail = email;
        this.advertiseTelegram = telegram;
        this.advertiseCity = city;
        this.advertisePrice = price;
    }

    public void setAdvertiseId(int advertiseId) {
        this.advertiseId = advertiseId;
    }

    public int getAdvertiseId() {
        return advertiseId;
    }

    public void setAdvertiseDate(int advertiseDate) {
        this.advertiseDate = advertiseDate;
    }

    public int getAdvertiseDate() {
        return advertiseDate;
    }

    public void setAdvertiseSub(String advertiseSub) {
        this.advertiseSub = advertiseSub;
    }

    public String getAdvertiseSub() {
        return advertiseSub;
    }

    public void setAdvertiseContent(String advertiseContent) {
        this.advertiseContent = advertiseContent;
    }

    public String getAdvertiseContent() {
        return advertiseContent;
    }

    public void setAdvertiseCategory(String advertiseCategory) {
        this.advertiseCategory = advertiseCategory;
    }

    public String getAdvertiseCategory() {
        return advertiseCategory;
    }

    public void setAdvertisePic(String advertisePic) {
        this.advertisePic = advertisePic;
    }

    public String getAdvertisePic() {
        return advertisePic;
    }

    public void setAdvertisePhone(String advertisePhone) {
        this.advertisePhone = advertisePhone;
    }

    public String getAdvertisePhone() {
        return advertisePhone;
    }

    public void setAdvertiseEmail(String advertiseEmail) {
        this.advertiseEmail = advertiseEmail;
    }

    public String getAdvertiseEmail() {
        return advertiseEmail;
    }

    public void setAdvertiseTelegram(String advertiseTelegram) {
        this.advertiseTelegram = advertiseTelegram;
    }

    public String getAdvertiseTelegram() {
        return advertiseTelegram;
    }

    public void setAdvertiseCity(String advertiseCity) {
        this.advertiseCity = advertiseCity;
    }

    public String getAdvertiseCity() {
        return advertiseCity;
    }

    public void setAdvertisePrice(String advertisePrice) {
        this.advertisePrice = advertisePrice;
    }

    public String getAdvertisePrice() {
        return advertisePrice;
    }
}
