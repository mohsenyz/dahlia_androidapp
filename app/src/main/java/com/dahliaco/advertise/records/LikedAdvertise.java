package com.dahliaco.advertise.records;

import com.orm.SugarRecord;

/**
 * Created by dahlia on 8/23/16.
 */
public class LikedAdvertise extends SugarRecord{
    private int key;
    public LikedAdvertise(){

    }
    public LikedAdvertise(int _key){
        this.key = _key;
    }
    public void setKey(int _key){
        this.key = _key;
    }
    public int getKey(){
        return this.key;
    }
}
