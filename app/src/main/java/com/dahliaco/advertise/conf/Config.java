package com.dahliaco.advertise.conf;

/**
 * Created by dahlia on 8/2/16.
 */
public class Config {
    public static final String APP_URL = "http://khoonehbazar.ir/";
    public static final String URL_GET_ADVERTISE = APP_URL + "test/app";
    public static final String URL_GET_DRAWER_VIEW = APP_URL + "asset/android/drawer.png";

    public static final String MINIDB_NOT_FIRST_VIEW = "IS_FIRST_VISIT";
}
