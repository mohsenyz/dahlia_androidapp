package com.dahliaco.advertise;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.dahliaco.advertise.adapter.ProfileObj;
import com.squareup.picasso.Picasso;
/**
 * Created by echessa on 7/24/15.
 */
public class ProfileListAdapter extends RecyclerView.Adapter<ProfileListAdapter.ViewHolder> {

    private List<ProfileObj> mItems;
    public ProfileListAdapter(List<ProfileObj> items) {
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.profile_list_adapter, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final ProfileObj item = mItems.get(i);
        viewHolder.title.setText(item.getTitle());
        viewHolder.li.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
        Picasso.with(viewHolder.title.getContext()).load(item.getImage()).into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;
        private final TextView title;
        private final LinearLayout li;
        ViewHolder(View v) {
            super(v);
            imageView = (ImageView)v.findViewById(R.id.image);
            title = (TextView) v.findViewById(R.id.title);
            li = (LinearLayout) v.findViewById(R.id.parent);
        }
    }

}
