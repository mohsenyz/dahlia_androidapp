package com.dahliaco.advertise.widget;

/**
 * Created by dahlia on 8/27/16.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

/**
 * Created by dahlia on 8/27/16.
 */
public class Reveal extends View {
    private static int radius = 0;
    private static boolean b = true;
    private static boolean isDestroyed = false;

    public void setDestroyed(boolean d){
        isDestroyed = d;
    }
    public Reveal(Context c){
        super(c);
        init();
    }

    public Reveal(Context c, AttributeSet a, int def){
        super(c, a, def);
        init();
    }

    public Reveal(Context c, AttributeSet a){
        super(c, a);
        init();
    }

    private void init(){
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "SFgsg", Toast.LENGTH_SHORT).show();
                radius = 0;
                invalidate();
            }
        });
    }
    final Paint p = new Paint();
    final Handler h = new Handler();

    /*@Override
    protected boolean drawChild(Canvas c, View bc, long d){
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.parseColor("#297dce"));
        p.setAntiAlias(true);
        p.setStrokeWidth(1);
        return true;
    }*/
    @Override
    protected void onDraw(final Canvas c){
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.parseColor("#297dce"));
        p.setAntiAlias(true);
        p.setStrokeWidth(1);
        if(!isDestroyed){
            if (b){
                drawIn(c);
            }else{
                drawOut(c);
            }
        }else{
            h.removeCallbacksAndMessages(null);
        }
    }

    private void drawIn(final Canvas c){
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(radius < 501){
                    radius = radius + 50;
                    c.drawCircle(0, 0, radius, p);
                    invalidate();
                }else {
                    b = false;
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            invalidate();
                        }
                    }, 2000);
                }
            }
        },16);
    }

    private void drawOut(final Canvas c){
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(radius > -1){
                    radius = radius - 50;
                    c.drawCircle(getMeasuredWidth(), getMeasuredHeight(), radius, p);
                    invalidate();
                }else {
                    b = true;
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            invalidate();
                        }
                    }, 2000);
                }
            }
        },16);
    }
}