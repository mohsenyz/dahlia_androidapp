package com.dahliaco.advertise.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by dahlia on 8/13/16.
 */
public class DahliaEditText extends EditText {

    public DahliaEditText(Context c, AttributeSet attr, int def){
        super(c, attr, def);
        init();
    }
    public DahliaEditText(Context c, AttributeSet attr){
        super(c, attr);
        init();
    }
    public DahliaEditText(Context c){
        super(c);
        init();
    }
    private void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"iran.ttf");
        setTypeface(tf);
    }
}
