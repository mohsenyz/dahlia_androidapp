package com.dahliaco.advertise.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.renderscript.Type;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by dahlia on 8/13/16.
 */
public class DahliaText extends TextView {

    public DahliaText(Context c, AttributeSet attr, int def){
        super(c, attr, def);
        init();
    }
    public DahliaText(Context c, AttributeSet attr){
        super(c, attr);
        init();
    }
    public DahliaText(Context c){
        super(c);
        init();
    }
    private void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"iran.ttf");
        setTypeface(tf);
    }
}
