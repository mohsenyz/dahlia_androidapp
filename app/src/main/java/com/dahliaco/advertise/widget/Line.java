package com.dahliaco.advertise.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.MonthDisplayHelper;
import android.view.View;

/**
 * Created by dahlia on 8/24/16.
 */
public class Line extends View {
    public Line(Context c){
        super(c);
    }

    public Line(Context c, AttributeSet a){
        super(c, a);
    }

    public Line(Context c, AttributeSet a, int d){
        super(c, a, d);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.parseColor("#1c6dbc"));
        p.setStrokeWidth(4);
        p.setAntiAlias(true);
        p.setShadowLayer(4, 0, 0, Color.parseColor("#555555"));
        setLayerType(LAYER_TYPE_SOFTWARE, p);
        canvas.drawLine(0, 0, getMeasuredWidth(), 0, p);
        super.onDraw(canvas);
    }
}
