package com.dahliaco.advertise.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by dahlia on 8/19/16.
 */
public class DahliaBtn extends Button {

    public DahliaBtn(Context c, AttributeSet attr, int def){
        super(c, attr, def);
        init();
    }
    public DahliaBtn(Context c, AttributeSet attr){
        super(c, attr);
        init();
    }
    public DahliaBtn(Context c){
        super(c);
        init();
    }
    private void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"iran.ttf");
        setTypeface(tf);
    }
}
