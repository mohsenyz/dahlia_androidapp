package com.dahliaco.advertise.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.renderscript.Type;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.dahliaco.advertise.helper.NumberFormatting;

/**
 * Created by dahlia on 8/13/16.
 */
public class DahliaTimer extends TextView {
    private static int max = 90;
    private boolean isFinish = true;
    Handler mHandler = new Handler();
    onFinishListener onFinish = new onFinishListener() {
        @Override
        public void onFinish() {
        }
    };
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            max = max - 1;
            String empty = "";
            if (max % 60 < 10){
                empty = NumberFormatting.englishToArabic("0");
            }
            setText(NumberFormatting.englishToArabic(Integer.toString((int)max / 60)) + ":" + empty + NumberFormatting.englishToArabic(Integer.toString(max % 60)));
            if (max != 0 && !isFinish){
                mHandler.postDelayed(this, 1000);
            }else{
                onFinish.onFinish();
                finishTimer();
            }
        }
    };

    public void setOnFinishListener(onFinishListener o){
        onFinish = o;
    }

    public DahliaTimer(Context c, AttributeSet attr, int def){
        super(c, attr, def);
        init();
    }
    public void setMaxTime(int second){
        this.max = second;
    }
    public DahliaTimer(Context c, AttributeSet attr){
        super(c, attr);
        init();
    }
    public DahliaTimer(Context c){
        super(c);
        init();
    }
    private void init(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"iran.ttf");
        setTypeface(tf);
    }
    public void startTimer(){
        isFinish = false;
        mHandler.postDelayed(mRunnable, 1000);
    }
    public void finishTimer(){
        isFinish = true;
        max = 0;
    }
    public interface onFinishListener{
        public void onFinish();
    }
}
