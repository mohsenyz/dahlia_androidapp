package com.dahliaco.advertise.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.dahliaco.advertise.R;

import org.w3c.dom.Text;

public class InfoDialogFragment extends DialogFragment {

    private TextView email;
    private TextView phone;
    private TextView tel;

    public InfoDialogFragment() {
        // Empty constructor is required for DialogFragment

        // Make sure not to add arguments to the constructor

        // Use `newInstance` instead as shown below

    }

    public static InfoDialogFragment newInstance(String phone , String email, String telegram) {
        InfoDialogFragment frag = new InfoDialogFragment();
        Bundle args = new Bundle();
        args.putString("phone", phone);
        args.putString("email", email);
        args.putString("telegram", telegram);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.info_fragment, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view

        phone = (TextView) view.findViewById(R.id.phone);
        email = (TextView) view.findViewById(R.id.email);
        tel = (TextView) view.findViewById(R.id.telegram);
        // Fetch arguments from bundle and set title

        String phones = getArguments().getString("phone");
        String emails = getArguments().getString("email");
        String tels = getArguments().getString("telegram");
        tel.setText(tels);
        email.setText(emails);
        phone.setText(phones);
        // Show soft keyboard automatically and request focus to field
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}