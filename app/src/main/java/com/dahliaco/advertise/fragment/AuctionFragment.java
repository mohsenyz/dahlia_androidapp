package com.dahliaco.advertise.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.dahliaco.advertise.AppController;
import com.dahliaco.advertise.AuctionListAdapter;
import com.dahliaco.advertise.DesignDemoRecyclerAdapter;
import com.dahliaco.advertise.R;
import com.dahliaco.advertise.adapter.AuctionObj;
import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.MultiScreens;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dahlia on 8/2/16.
 */
public class AuctionFragment extends Fragment {
    public static String TAB_NAME = "مزایده ها";
    private static final String TAB_POSITION = "tab_position";
    private RecyclerView recyclerView;
    private ArrayList<AuctionObj> items;
    private AuctionListAdapter adapter;
    private static SwipeRefreshLayout swipe;
    private Context m;

    public AuctionFragment(){

    }

    public static AuctionFragment newInstance(int tabPosition) {
        AuctionFragment fragment = new AuctionFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);

        items = new ArrayList<AuctionObj>();
        View v =  inflater.inflate(R.layout.fragment_list_view, container, false);
        swipe = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        swipe.setColorSchemeColors(Color.BLUE, Color.YELLOW, Color.GREEN);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AuctionListAdapter(items);
        recyclerView.setAdapter(adapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        itemAnimator.setMoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        swipe.post(new Runnable(){
            public void run(){
                swipe.setRefreshing(true);
                getAdapter();
            }
        });
        boolean isTablet = getActivity().getResources().getBoolean(R.bool.isTablet);
        if (MultiScreens.isTablet(getActivity()) || isTablet) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        return v;
    }

    public void getAdapter(){
        JsonArrayRequest req = new JsonArrayRequest(Config.URL_GET_ADVERTISE,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        swipe.setRefreshing(false);
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject adv = (JSONObject) response
                                        .get(i);
                                String pic = adv.getString("Pic");
                                Log.i("pic", pic);
                                if(pic.contains(",")){
                                    for ( String pict : pic.split(",")) {
                                        if(pict.length() > 3){
                                            pic = pict;
                                        }
                                    }
                                }
                                AuctionObj obj= new AuctionObj();
                                obj.setTitle(adv.getString("Sub"));
                                obj.setPrice(adv.getString("Price"));
                                obj.setCity(adv.getString("City"));
                                obj.setContent(adv.getString("Content"));
                                obj.setId(Integer.parseInt(adv.getString("Id")));
                                obj.setPhone(adv.getString("Phone"));
                                obj.setEmail(adv.getString("email"));
                                obj.setTel(adv.getString("telegram"));
                                obj.setDate(adv.getInt("Date"));
                                obj.setCategory(adv.getString("Category"));
                                obj.setImageId(pic);
                                items.add(obj);
                                adapter.notifyDataSetChanged();
                                swipe.setRefreshing(false);
                            }
                            swipe.setRefreshing(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

}
