package com.dahliaco.advertise.fragment;

/**
 * Created by dahlia on 8/2/16.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.dahliaco.advertise.AppController;
import com.dahliaco.advertise.DesignDemoRecyclerAdapter;
import com.dahliaco.advertise.R;
import com.dahliaco.advertise.SecondActivityDialog;
import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.conf.Config;
import com.dahliaco.advertise.helper.CachedManager;
import com.dahliaco.advertise.helper.GZipRequest;
import com.dahliaco.advertise.helper.MultiScreens;
import com.dahliaco.advertise.helper.PreCacheLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by dahlia on 8/2/16.
 */
public class AdvertiseFragment extends Fragment {
    private static final String TAB_POSITION = "tab_position";
    private RecyclerView recyclerView;
    private ArrayList<ListObj> items;
    private static SwipeRefreshLayout swipe;
    private Context m;
    public static String TAB_NAME = "آگهی ها";

    public AdvertiseFragment() {

    }

    public static AdvertiseFragment newInstance(int tabPosition) {
        AdvertiseFragment fragment = new AdvertiseFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }

    public void change(String atr){
        atr = "Ali";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);
        byte[] al = new byte[1024];
        String a = "dgdfgfh";
        change(a);

        items = new ArrayList<ListObj>();
        View v =  inflater.inflate(R.layout.fragment_list_view, container, false);
        swipe = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        swipe.setColorSchemeColors(Color.BLUE, Color.YELLOW, Color.GREEN);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        PreCacheLayoutManager pr = new PreCacheLayoutManager(getActivity());
        pr.setExtraLayoutSpace(getActivity().getWindow().getDecorView().getMeasuredHeight());
        recyclerView.setLayoutManager(pr);
        AppController.adapter = new DesignDemoRecyclerAdapter(items);
        AppController.adapter.setOnClickListener(new DesignDemoRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, ListObj o, int i) {

            }
        });
        AppController.adapter.setOnItemSelectStateListener(AppController.onItemSelectStateChangedListener);
        AppController.adapter.setOnLongClickListener(new DesignDemoRecyclerAdapter.OnLongClickListener() {
            @Override
            public void onLongClick(View v, ListObj o, int i) {
                if(!AppController.adapter.IS_SELECTED){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppController.adapter.popupWindow.dismiss();
                        }
                    }, 1000);
                    AppController.adapter.setIsSelected(true);
                    AppController.adapter.notifyDataSetChanged();
                }
            }
        });
        recyclerView.setAdapter(AppController.adapter);
        swipe.post(new Runnable(){
            public void run(){
                swipe.setRefreshing(true);
                getAdapter();
            }
        });
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        itemAnimator.setMoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 10){
                    ((DesignDemoRecyclerAdapter)recyclerView.getAdapter()).popupWindow.dismiss();
                }
            }
        });
        boolean isTablet = getActivity().getResources().getBoolean(R.bool.isTablet);
        if (MultiScreens.isTablet(getActivity()) || isTablet) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        return v;
    }



    public void parseJsonToList(JSONArray response){
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject adv = (JSONObject) response
                        .get(i);
                String pic = adv.getString("Pic");
                if(pic.contains(",")){
                    for ( String pict : pic.split(",")) {
                        if(pict.length() > 3){
                            pic = pict;
                        }
                    }
                }
                ListObj obj= new ListObj();
                obj.setTitle(adv.getString("Sub"));
                obj.setPrice(adv.getString("Price"));
                obj.setCity(adv.getString("City"));
                obj.setContent(adv.getString("Content"));
                obj.setId(Integer.parseInt(adv.getString("Id")));
                obj.setPhone(adv.getString("Phone"));
                obj.setEmail(adv.getString("email"));
                obj.setTel(adv.getString("telegram"));
                obj.setDate(adv.getInt("Date"));
                obj.setCategory(adv.getString("Category"));
                obj.setImageId(pic);
                items.add(obj);
                //AppController.adapter.notifyItemChanged(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void getAdapter(){
        Cache.Entry e = AppController.getInstance().getRequestQueue().getCache().get(Config.URL_GET_ADVERTISE);
        if(e != null){
            try {
                new LoadJson().execute(new JSONArray(new String(e.data)));
            }catch (Exception v){
                v.printStackTrace();
            }
        }else{
            GZipRequest req = new GZipRequest(Config.URL_GET_ADVERTISE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                new LoadJson().execute(new JSONArray(response));
                            }catch (Exception e){
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    swipe.setRefreshing(false);
                }
            }){
                @Override
                public Priority getPriority() {
                    return Priority.HIGH;
                }
            };
            req.setShouldCache(true);
            AppController.getInstance().addToRequestQueue(req);
        }
    }


    private class LoadJson extends AsyncTask<JSONArray, Void, Boolean>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(JSONArray... params) {
            parseJsonToList(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            AppController.adapter.notifyDataSetChanged();
            Log.i("list have to load", "yeah");
            swipe.setRefreshing(false);
            super.onPostExecute(aBoolean);
        }
    }


}
