package com.dahliaco.advertise.fragment;

/**
 * Created by dahlia on 8/2/16.
 */

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.dahliaco.advertise.AppController;
import com.dahliaco.advertise.DesignDemoRecyclerAdapter;
import com.dahliaco.advertise.ProfileListAdapter;
import com.dahliaco.advertise.R;
import com.dahliaco.advertise.adapter.ListObj;
import com.dahliaco.advertise.adapter.ProfileObj;
import com.dahliaco.advertise.conf.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dahlia on 8/2/16.
 */
public class ProfileFragment extends Fragment {
    public static String TAB_NAME = "ناحیه کاربری";
    private static final String TAB_POSITION = "tab_position";
    private RecyclerView recyclerView;
    private ArrayList<ProfileObj> items;
    private ProfileListAdapter adapter;
    private static SwipeRefreshLayout swipe;

    public ProfileFragment() {

    }

    public static ProfileFragment newInstance(int tabPosition) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        int tabPosition = args.getInt(TAB_POSITION);

        items = new ArrayList<ProfileObj>();
        View v =  inflater.inflate(R.layout.fragment_list_view, container, false);
        swipe = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        swipe.setEnabled(false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.myadvertise), R.drawable.ic_advertise_auction));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.myauction), R.drawable.ic_advertise_auction));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.conversations), R.drawable.ic_conversations));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.lastvisited), R.drawable.ic_lastvisited));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.liked), R.drawable.ic_liked));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.offlined), R.drawable.ic_offlined));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.rules), R.drawable.ic_rules));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.support), R.drawable.ic_support));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.aboutus), R.drawable.ic_aboutus));
        items.add(new ProfileObj(getActivity().getResources().getString(R.string.logout), R.drawable.ic_unlock));
        adapter = new ProfileListAdapter(items);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        itemAnimator.setMoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        return v;
    }
}
