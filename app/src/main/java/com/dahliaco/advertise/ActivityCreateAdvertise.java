package com.dahliaco.advertise;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dahliaco.advertise.helper.ValidatorHelper;

public class ActivityCreateAdvertise extends AppCompatActivity {
    TextInputLayout subjectLayout, contentLayout;
    TextView subject, content;
    LinearLayout first, twice, thirth;
    Animation moveInLeft, moveOutRight, moveOutRight1;
    ImageView image1, image2, image3;
    Button next, selectCategory, selectTimeToShow;
    static int LEVEL = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createadvertise);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        subjectLayout = (TextInputLayout) findViewById(R.id.subject_layout);
        contentLayout = (TextInputLayout) findViewById(R.id.content_layout);
        subject = (TextView) findViewById(R.id.subject);
        content = (TextView) findViewById(R.id.content);
        first = (LinearLayout) findViewById(R.id.first);
        twice = (LinearLayout) findViewById(R.id.twice);
        thirth = (LinearLayout) findViewById(R.id.thirth);
        moveInLeft = AnimationUtils.loadAnimation(this, R.anim.move_in_left);
        moveOutRight = AnimationUtils.loadAnimation(this, R.anim.move_out_right);
        moveOutRight1 = AnimationUtils.loadAnimation(this, R.anim.move_out_right);
        next = (Button) findViewById(R.id.next);
        //twice
        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.image2);
        image3 = (ImageView) findViewById(R.id.image3);
        //thirth
        selectCategory = (Button) findViewById(R.id.selectcategory);
        selectTimeToShow = (Button) findViewById(R.id.selecttimetoshow);
        //-----
        subject.addTextChangedListener(new Validator(subject));
        content.addTextChangedListener(new Validator(content));
        selectCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ActivityCreateAdvertise.this,  SelectCategoryActivity.class), 4);
            }
        });
        //-----
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (LEVEL){
                    case 1:
                        goToTwiceLevel();
                        break;
                    case 2:
                        goToThirthLevel();
                        break;
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        LEVEL = 1;
        first.setVisibility(View.VISIBLE);


        //---------

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        });


        //------


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestFocus(View v){
        if(v.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateSubject(){
        if(ValidatorHelper.isEmpty(subject.getText().toString())){
            subjectLayout.setError(getString(R.string.rule_subject));
            requestFocus(subject);
            return false;
        }else{
            subjectLayout.setErrorEnabled(false);
        }
        return true;
    }


    private boolean validateContent(){
        if(ValidatorHelper.isEmpty(content.getText().toString()) || !ValidatorHelper.moreThanOrEqual(content.getText().toString(), 20)){
            contentLayout.setError(getString(R.string.rule_content));
            requestFocus(content);
            return false;
        }else{
            contentLayout.setErrorEnabled(false);
        }
        return true;
    }

    private void goToTwiceLevel(){
        if(!validateSubject())
            return;
        if(!validateContent())
            return;
        moveOutRight.setAnimationListener(new AnimationListeners().goToTwicePage());
        first.startAnimation(moveOutRight);
        LEVEL = 2;
    }

    private void goToThirthLevel(){
        moveOutRight.setAnimationListener(new AnimationListeners().goToThirthPage());
        twice.startAnimation(moveOutRight);
        LEVEL = 3;
    }
    private class Validator implements TextWatcher{
        private View view;
        public Validator(View v){
            view = v;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.subject:
                    validateSubject();
                    break;

                case R.id.content:
                    validateContent();
                    break;
            }
        }
    }



    private class AnimationListeners{
        public Animation.AnimationListener goToTwicePage(){
            return new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    first.setVisibility(View.GONE);
                    twice.setVisibility(View.VISIBLE);
                    twice.startAnimation(moveInLeft);
                    moveOutRight.setAnimationListener(null);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            };
        }

        public Animation.AnimationListener goToThirthPage(){
            return new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    twice.setVisibility(View.GONE);
                    thirth.setVisibility(View.VISIBLE);
                    thirth.startAnimation(moveInLeft);
                    moveOutRight.setAnimationListener(null);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            };
        }
    }
}
